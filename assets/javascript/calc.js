$().ready(function () {


    function calc(){
        var calc = {
            buyers: $("input[name='buyers']").val(),
            field: $("input[name='field']:checked").val(),
            providers: $("input[name='providers']").val(),
            employees: $("input[name='employees']").val(),
            organization: $("input[name='organization']:checked").val(),
            tax: $("input[name='tax']:checked").val()
        };
        var formText = {
            buyers: $("input[name='buyers']").val(),
            field: 'не выбранно',
            providers: $("input[name='providers']").val(),
            employees: $("input[name='employees']").val(),
            organization: 'не выбранно',
            tax: 'не выбранно'
        }
        var price;
        var operations = +calc.buyers + +calc.providers;

        if ($("input[name='field']").is(":checked") && ($("input[name='organization']").is(":checked")) && ($("input[name='tax']").is(":checked"))) {
            if (operations < 401) {
                switch (calc.field) {
                    case 'services':
                    case 'wholesale':
                    case 'retail':
                        price = 1000;
                        break;
                    case 'representation':
                    case 'non-profit':
                        price = 3000;
                        break;
                    case 'production':
                    case 'build':
                        price = 9000;
                        break;
                }
                ;
                if (calc.field == 'non-profit'){
                    price +=0;
                }
                else{
                    switch (calc.organization) {
                        case 'ooo':
                            price += 1000;
                            break;
                        case 'ao':
                            price += 1000;
                            break;
                    };
                };

                if (calc.field == 'non-profit') {
                    price += 0;
                }
                else{
                    switch (calc.tax) {
                        case 'usn15':
                            if ((calc.field == 'build') || calc.field == 'production'){
                                price += 0;
                            }
                            else{
                                price += 500;
                            }
                            break;
                        case 'nds':
                            if ((calc.field == 'production') || (calc.field == 'build') ){
                                price += 0;
                            }
                            else{
                                if (calc.organization == 'ooo'){
                                    price += 1000;
                                }
                                else {
                                    price += 500;
                                }
                            }
                            break;
                        case 'patent':
                            if (calc.organization == 'ip'){
                                price -= 500;
                            }
                            else{
                                price = "Патент только для ИП"
                            }
                            break;
                    }
                    ;
                };


                if ((calc.organization !== 'ip') && (calc.tax == 'patent')){
                    price = "Патент только для ИП";
                }
                else{
                   if( +calc.employees > 1 ){
                       price = price + (calc.employees-1) * 1000 + 2000 + operations*500 + ' Рублей/месяц';

                   }
                   else if ( +calc.employees == 1 ){
                       price = price + 2000 + operations*500 + ' Рублей/месяц';
                   }else{
                       price = price + operations*500 + ' Рублей/месяц';
                   }
                }

            } else {
                price = "От 400 операций цена по договоренности";
            };
        }
        else{
            price = "Выберите все условия";
        };
        $('#cnumb').html(price);

        switch (calc.field){
            case 'services':
                formText.services = "Услуги";
                break;
            case 'wholesale':
                formText.services = "Оптовая торговля";
                break;
            case 'retail':
                formText.services = "Розничная торговля";
                break;
            case 'representation':
                formText.services = "Пред-во иностранной ком.";
                break;
            case 'non-profit':
                formText.services = "Некоммерческая деятельность";
                break;
            case 'production':
                formText.services = "Производство";
                break;
            case 'build':
                formText.services = "Строительство";
                break;
        };
        switch (calc.organization){
            case 'ooo':
                formText.organization = "OOO";
                break;
            case 'ao':
                formText.organization = "НКО";
                break;
            case 'ip':
                formText.organization = "ИП";
                break;
        };
        switch (calc.tax){
            case 'usn15':
                formText.tax = "УСН 15%";
                break;
            case 'usn6':
                formText.tax = "УСН 6%";
                break;
            case 'nds':
                formText.tax = "ОСНО";
                break;
        };


        $('#formServices').html(formText.services);
        $('#formOrganization').html(formText.organization);
        $('#formTax').html(formText.tax);
        $('#formBuyers').html(formText.buyers);
        $('#formProviders').html(formText.providers);
        $('#formEmployees').html(formText.employees);
        $('#formPrice').html(price);

        $("input[id='first-hidden']").val(formText.services);
        $("input[id='second-hidden']").val(formText.organization);
        $("input[id='third-hidden']").val(formText.tax);
        $("input[id='four-hidden']").val(formText.buyers);
        $("input[id='fifth-hidden']").val(formText.providers);
        $("input[id='six-hidden']").val(formText.employees);
        $("input[id='seven-hidden']").val(price);

    }
    $("input[name='field'],input[name='organization'],input[name='tax'] ").on('click', calc);
    $("input[name='buyers'],input[name='providers'],input[name='employees']").on('keyup', calc);


});