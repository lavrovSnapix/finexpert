$().ready(function () {

    $("a.scroll-anchor").click(function () {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        return false;
    });

    $('.single-item').not('.slick-initialized').slick({
        autoplay: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    arrows: false
                }
            },
        ]
    });

    $('html').click(function() {
        $('.right-nav').removeClass('right-nav_active');
        $('.fixed_line').removeClass('fixed_line_rotate');
    });

    $('.fixed_nav').click(function(event){
        event.stopPropagation();
    });

    $('.fixed_nav').click(function () {
        $('.right-nav').addClass('right-nav_active');
        $('.fixed_line').addClass('fixed_line_rotate');
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 120) {
            $(".fixed_nav").addClass('active_fixed_nav')
        }
        else {
            $(".fixed_nav").removeClass('active_fixed_nav');
        }


        var position_advantages = $('.advantages').offset(),
            position_map = $('.map').offset(),
            advantages_height = $('.advantages').height(),
            position_margin = 100;
        if (window.screen.width < 768) {
            position_margin = 50;
        }
        if ((($(window).scrollTop() > position_advantages.top - position_margin) && ($(window).scrollTop() < position_advantages.top + advantages_height)) || ($(window).scrollTop() > position_map.top - position_margin)) {
            $('.fixed_nav').addClass('fixed_nav_black')
        }
        else {
            $('.fixed_nav').removeClass('fixed_nav_black')
        }

    });


    if (window.screen.width < 768) {
        $('.fixed_nav').click(function () {
            $('.fixed_nav').toggleClass('fixed_nav_width')
        })
    }
    ;

    $('.click-modal').click(function () {
        $('.modal-form-overlay').addClass('modal-form-overlay_active')
    })

    $('.modal-btn-close').click(function () {
        $('.modal-form-overlay').removeClass('modal-form-overlay_active')
    })

    $('.ontop').click(function () {
        $('html, body').animate({scrollTop: 0}, 1500);
        return false;
    });

    $('.calc-modal').click(function () {
        $('.popup-calculate').addClass('popup-calculate-active');
    });
    $('.popup-calculate-close').click(function () {
        $('.popup-calculate').removeClass('popup-calculate-active');
    })

    $('.single-logo').not('.slick-initialized').slick({
        autoplay: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    arrows: false
                }
            },
        ]
    });

    $.fn.setCursorPosition = function(pos) {
        if ($(this).get(0).setSelectionRange) {
            $(this).get(0).setSelectionRange(pos, pos);
        } else if ($(this).get(0).createTextRange) {
            var range = $(this).get(0).createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    };

    $('.phone-mask').click(function(){
        if ($(this).val() === '+7(___) ___-__-__') {
            $(this).setCursorPosition(3);
        }
    });

    $(function(){
        $(".phone-mask").mask("+7(999) 999-99-99", {autoclear: false})
    });


});